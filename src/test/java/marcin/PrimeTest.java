package marcin;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class PrimeTest {

    static Prime prime = null;

    @BeforeAll
    static void beforeAll() {
        prime = new Prime();
    }
/*    @BeforeEach
    void beforeEach(){
        prime = new Prime();
    }*/

    @Test
    void isPrime() {
        //given
//        prime = new Prime(); testy sa zalezne przez to
        //when
        boolean prime1 = prime.isPrime(1);
        //then
        assertFalse(prime1);

//        assertEquals(false, prime1);
//        assertArrayEquals();
    }

    @Test
    void isPrime2() {
        boolean prime1 = prime.isPrime(2);

        assertTrue(prime1);
    }

    @Test
    void shouldThrowExceptionWhenNIsNegative() {
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class, () -> prime.isPrime(-15));
        String message = illegalArgumentException.getMessage();

        assertEquals("N is negative", message);
    }

    @Test
    void getTimeDiffs(){
        LocalDateTime localDateTime = LocalDateTime.of(2019,8, 10, 10, 04);
        LocalDateTime localDateTime2 = LocalDateTime.of(2019,8, 12, 9, 03);

        long hours = localDateTime.until(localDateTime2, ChronoUnit.HOURS);
        long days = localDateTime.until(localDateTime2, ChronoUnit.DAYS);

        assertEquals(46, hours);
        assertEquals(1, days);
    }
}