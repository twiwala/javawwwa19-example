package patryk.singleton;

public class SingletonEager {
    private static final SingletonEager instance = new SingletonEager();

    private SingletonEager() {
        System.out.println("Hello :))");
    }

    public static SingletonEager getInstance() {
        return instance;
    }


}
