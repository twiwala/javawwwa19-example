package patryk.singleton;

import patryk.builder.Samochod;

import java.util.List;
import java.util.Map;

//Joshua Bloch
public enum SingletonEnum {
    INSTANCE;

    private List<Samochod> cars;

    public List<Samochod> getCars() {
        return cars;
    }
}
