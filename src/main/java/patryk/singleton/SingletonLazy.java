package patryk.singleton;

public class SingletonLazy {

    private static SingletonLazy instance = null;

    private SingletonLazy() {
        System.out.println("Hello :))");
    }

    public static SingletonLazy getInstance() {
        if (instance == null) {
            synchronized (SingletonLazy.class) {
                if (instance == null) {
                    instance = new SingletonLazy();
                    return instance;
                }
            }
        }
        return instance;
    }
}
