package patryk.builder;

import lombok.Builder;

public class Samochod {
    private String marka;
    private String model;
    private double silnik;

    private int liczbaDrzwi;
    private String vin;
    private boolean klimatyzacja;
    private boolean skrzyniaAutomatyczna;

    private Samochod(String marka, String model, double silnik, int liczbaDrzwi, String vin, boolean klimatyzacja, boolean skrzyniaAutomatyczna) {
        this.marka = marka;
        this.model = model;
        this.silnik = silnik;
        this.liczbaDrzwi = liczbaDrzwi;
        this.vin = vin;
        this.klimatyzacja = klimatyzacja;
        this.skrzyniaAutomatyczna = skrzyniaAutomatyczna;
    }

    public static SamochodBuilder builder(String marka, String model, double silnik) {
        return new SamochodBuilder(marka, model, silnik);
    }

    public static class SamochodBuilder {
        private String marka;
        private String model;
        private double silnik;
        private int liczbaDrzwi;
        private String vin;
        private boolean klimatyzacja;
        private boolean skrzyniaAutomatyczna;

        SamochodBuilder(String marka, String model, double silnik) {
            this.marka = marka;
            this.model = model;
            this.silnik = silnik;
        }

        public SamochodBuilder marka(String marka) {
            this.marka = marka;
            return this;
        }

        public SamochodBuilder model(String model) {
            this.model = model;
            return this;
        }

        public SamochodBuilder silnik(double silnik) {
            this.silnik = silnik;
            return this;
        }

        public SamochodBuilder liczbaDrzwi(int liczbaDrzwi) {
            this.liczbaDrzwi = liczbaDrzwi;
            return this;
        }

        public SamochodBuilder vin(String vin) {
            this.vin = vin;
            return this;
        }

        public SamochodBuilder klimatyzacja(boolean klimatyzacja) {
            this.klimatyzacja = klimatyzacja;
            return this;
        }

        public SamochodBuilder skrzyniaAutomatyczna(boolean skrzyniaAutomatyczna) {
            this.skrzyniaAutomatyczna = skrzyniaAutomatyczna;
            return this;
        }

        public Samochod build() {
            return new Samochod(marka, model, silnik, liczbaDrzwi, vin, klimatyzacja, skrzyniaAutomatyczna);
        }

        public String toString() {
            return "Samochod.SamochodBuilder(marka=" + this.marka + ", model=" + this.model + ", silnik=" + this.silnik + ", liczbaDrzwi=" + this.liczbaDrzwi + ", vin=" + this.vin + ", klimatyzacja=" + this.klimatyzacja + ", skrzyniaAutomatyczna=" + this.skrzyniaAutomatyczna + ")";
        }
    }









/*
    public Samochod(String marka, String model, double silnik, int liczbaDrzwi) {
        this.marka = marka;
        this.model = model;
        this.silnik = silnik;
        this.liczbaDrzwi = liczbaDrzwi;
    }

    public Samochod(String marka, String model, double silnik, String vin) {
        this.marka = marka;
        this.model = model;
        this.silnik = silnik;
        this.vin = vin;
    }

    public Samochod(String marka, String model, double silnik, boolean skrzyniaAutomatyczna) {
        this.marka = marka;
        this.model = model;
        this.silnik = silnik;
        this.skrzyniaAutomatyczna = skrzyniaAutomatyczna;
    }
    public Samochod(String marka, String model, double silnik, boolean klimatyzacja) {
        this.marka = marka;
        this.model = model;
        this.silnik = silnik;
        this.klimatyzacja = klimatyzacja;
    }

    public Samochod(String marka, String model, double silnik, int liczbaDrzwi, String vin, boolean klimatyzacja, boolean skrzyniaAutomatyczna) {
        this.marka = marka;
        this.model = model;
        this.silnik = silnik;
        this.liczbaDrzwi = liczbaDrzwi;
        this.vin = vin;
        this.klimatyzacja = klimatyzacja;
        this.skrzyniaAutomatyczna = skrzyniaAutomatyczna;
    }*/
}