package patryk.builder;

public class Person {
    private String name;
    private int age;
    private String pesel;

    //java beans
    public Person setName(String name) {
        this.name = name;
        return this;
    }

    public Person setAge(int age) {
        this.age = age;
        return this;
    }

    public Person setPesel(String pesel) {
        this.pesel = pesel;
        return this;
    }
}
