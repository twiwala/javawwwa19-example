package patryk.lambdy;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Ex1 {
    public static void main(String[] args) {

        List<String> stringList = Arrays.asList("Jan", "Katarzyna", "Sebek", "Marta");
        Collections.sort(stringList);
        System.out.println(stringList);

        Collections.sort(stringList, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.length() - o2.length();
            }
        });
        System.out.println(stringList);

        Collections.sort(stringList, (String o1, String o2) -> {
            return o1.length() - o2.length();
        });


        Collections.sort(stringList, (o1, o2) -> {
            return o1.length() - o2.length();
        });


        Collections.sort(stringList, (object1, object2) -> object1.length() - object2.length());


        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("HEllo world w 2 watku");
            }
        });


        Thread thread1 = new Thread(() -> System.out.println("HEllo world w 2 watku"));
    }
}
