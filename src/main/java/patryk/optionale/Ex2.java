package patryk.optionale;

import java.util.List;
import java.util.Optional;

public class Ex2 {
    public static void main(String[] args) {
        Optional<Integer> o = Optional.ofNullable(null);

//        o.get();
        o.ifPresentOrElse(e -> System.out.println(e), () -> System.out.println("Ups pusto :("));
        Integer integer = o.orElse(0);
        System.out.println(integer);

//        o.orElseThrow(() -> new IllegalArgumentException());
//        o.orElseGet()

        List<String> s = List.of();
        Optional<String> any = s.stream().findAny();
        System.out.println(any.isPresent());
    }
}
