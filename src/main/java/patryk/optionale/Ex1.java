package patryk.optionale;

import java.util.Optional;

public class Ex1 {
    public static void main(String[] args) {
//        Optional<Object> o = Optional.of(null);

//        Optional<Object> o1 = Optional.ofNullable(null);

        Optional<Integer> integer = Optional.ofNullable(5);
        integer.ifPresent(e -> System.out.println(e));
        if(integer.isPresent()){
            System.out.println("Ojej w srodku cos jest!");
        }
        Integer integer1 = integer.get();
        System.out.println(integer1);
    }
}
