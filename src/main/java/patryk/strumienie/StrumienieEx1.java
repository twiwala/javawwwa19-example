package patryk.strumienie;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StrumienieEx1 {
    public static void main(String[] args) {
        List<String> strings = List.of("Lorem", "Jan", "Magda", "Aga");
        System.out.println(longWords(strings));
        System.out.println(longWordsStream(strings));
        System.out.println(longWordsLengthStream(strings));
    }

    // [5, 3 , 5]
    private static List<Integer> longWordsLengthStream(List<String> strings) {
        return strings.stream()
                .map(e -> {
                    if (e.equals("Lorem"))
                        return e.length();
                    else {
                        return e.length()*2;
                    }
                })
                .filter(e -> e % 2 == 0)
                .sorted()
                .collect(Collectors.toList());
    }

    public static List<String> longWords(List<String> words) {
        ArrayList<String> strings = new ArrayList<>();
        for (String s : words) {
            if (s.length() > 4) {
                strings.add(s);
            }
        }
        return strings;
    }

    public static List<String> longWordsStream(List<String> words) {
        return words.stream()
                .filter(e -> e.length() > 4)
                .collect(Collectors.toList());
    }

}
