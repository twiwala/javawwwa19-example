 import java.util.ArrayList;
import java.util.List;

public class Database {
    private List<CarType> carTypes = new ArrayList<>();
//    private List<Car>
    private static Database database = new Database();
    public static final Database getInstance(){
        return database;
    }
    private Database(){
        carTypes.add(new CarType("A", true, 5, 150));
        carTypes.add(new CarType("B", true, 5, 200));
        carTypes.add(new CarType("C", false, 5, 300));
    }

/**
 Jaki samochod chcesz wypozyczyc???
 Car car = new Car();

// 0. A
 // 1. B

 // 2. C
 car.setType(carTypes.get(0));
 */
}
