import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public final class CarType {
    final String name;// = "Klasa a";
    final boolean skrzyniaBiegowAutomatyczna;// = false;
    final int iloscDrzwi;// = 5;
    final double cena;

    public CarType(String name, boolean skrzyniaBiegowAutomatyczna, int iloscDrzwi, double cena) {
        this.name = name;
        this.skrzyniaBiegowAutomatyczna = skrzyniaBiegowAutomatyczna;
        this.iloscDrzwi = iloscDrzwi;
        this.cena = cena;

        LocalDate localDate = LocalDate.of(1999, 10, 20);
        LocalDate localDate1 = LocalDate.parse("1999-10-20");

        LocalTime localTime = LocalTime.of(20, 20, 8);
//        LocalDateTime localDateTime = LocalDateTime.of();
    }
}
