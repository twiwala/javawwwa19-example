import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@AllArgsConstructor
@Builder
@Data
public class CarCapacity {
    int capacitySmallLuggage = 2;
    int capacityMidLuggage = 2;
    int capacityLargeLuggage = 2;
}
