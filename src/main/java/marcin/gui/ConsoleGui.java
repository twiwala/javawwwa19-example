package marcin.gui;

import marcin.Service;
import marcin.database.Database;
import marcin.model.Vehicle;

import java.util.Scanner;

public class ConsoleGui {
    private Service service = new Service();

    public void addCar() {
//        Scanner scanner = new Scanner(System.in);
        Vehicle.VehicleBuilder builder = Vehicle.builder();
        //jaka marka??
        builder.marka("BMW");
        //jaki silnik??
        builder.engine("1.2");

        Vehicle build = builder.build();
        service.addCar(build);
    }
}
