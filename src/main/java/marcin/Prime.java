package marcin;

public class Prime {
    public boolean isPrime(int n){
        if(n < 0){
            throw new IllegalArgumentException("N is negative");
        }
        if(n == 2) {
            return true;
        }
        return false;
    }
}
