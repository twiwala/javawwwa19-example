package marcin.model;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Vehicle {
    private String marka;
    private String engine;
}
