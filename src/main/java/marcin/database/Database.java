package marcin.database;

import marcin.model.Vehicle;

import java.util.ArrayList;
import java.util.List;

public class Database {
    private List<Vehicle> vehicles = new ArrayList<>();
    private static final Database instance = new Database();
    private Database(){

    }
    public static Database getInstance(){
        return instance;
    }

    public void addVehicle(Vehicle v){
        vehicles.add(v);
    }
    public List<Vehicle> getVehicles(){
        return new ArrayList<>(vehicles);
    }
}
